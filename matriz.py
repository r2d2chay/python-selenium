from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import requests
import re
from datetime import datetime, timedelta, time
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time



# URL del sitio web
url = "https://tasks.evalartapp.com/automatization/"

# Datos de inicio de sesión
username = "824033"
password = "10df2f32286b7120My0zLTMzMDQyOA==30e0c83e6c29f1c3"

# Configuración del navegador
options = webdriver.ChromeOptions()
options.add_argument("--start-maximized")  # Abre el navegador maximizado
driver = webdriver.Chrome(options=options)

# Abrir la página de inicio de sesión
driver.get(url)

# Localizar el campo de usuario e ingresar el nombre de usuario
username_field = driver.find_element(By.NAME, "username")
username_field.send_keys(username)

# Localizar el campo de contraseña e ingresar la contraseña
password_field = driver.find_element(By.NAME, "password")
password_field.send_keys(password)

# Enviar el formulario de inicio de sesión
password_field.send_keys(Keys.ENTER)

# Esperar a que la página se cargue después de iniciar sesión (puedes cambiar el tiempo de espera según sea necesario)
WebDriverWait(driver, 10).until(EC.url_to_be("https://tasks.evalartapp.com/automatization/buttons/test"))



num1=1
while num1 <= 11:

    html_content = driver.page_source
    # Crear un objeto BeautifulSoup para analizar el HTML
    soup = BeautifulSoup(html_content, 'html.parser')

    coordenadas = soup.find_all('p', class_='text-center text-xl font-bold')

    p_tags_array = []

    # Pasamos valores a array para mas comodidad 
    for p_tag in coordenadas:
        p_tags_array.append(p_tag)

    texto = str(p_tags_array[0])

    # Buscar lo que está después de ">" y antes de "<" usando expresiones regulares
    contenido = re.findall('>(.*?)<', texto)

    # Convertir el contenido en una lista
  
    lista_contenido = contenido[0].split('.') if contenido else []
 
    if len(lista_contenido) == 1:
        print("error en limitador de coordenadas")
        break 

    # Usar list comprehension para convertir cada string en una tupla evaluando la expresión con eval()
    coordenadas_tuples = [eval(coordenada_str) for coordenada_str in lista_contenido]
    print(coordenadas_tuples)

    # Crear una matriz vacía
    matriz = []

    # Obtener todos los botones dentro del div
    buttons = soup.find_all('button', class_='grid_button')

    # Inicializar el índice de fila y columna
    fila_actual = 0
    columna_actual = 0

    # Recorrer los botones y agregar sus valores a la matriz
    for button in buttons:
        # Obtener el valor del botón como entero
        valor = int(button['value'])

        # Convertir el valor a cadena con ceros a la izquierda si es necesario
        #valor_str = str(valor).zfill(3)

        # Agregar el valor a la fila actual de la matriz
        if len(matriz) <= fila_actual:
            matriz.append([])
        matriz[fila_actual].append(valor)

        # Incrementar el índice de columna
        columna_actual += 1

        # Si se llega al final de una fila, pasar a la siguiente fila
        if columna_actual == 12:
            fila_actual += 1
            columna_actual = 0

    # Imprimir la matriz
    for fila in matriz:
        print(fila)


    # Coordenadas a aplicar
    coordenadas = coordenadas_tuples

    # Posición inicial
    posicion_actual = (0, 0)

    # Aplicar las coordenadas
    for dx, dy in coordenadas:
        # Sumar los desplazamientos a las coordenadas actuales
        nueva_fila = posicion_actual[0] + dy
        nueva_columna = posicion_actual[1] + dx

        # Verificar si la nueva posición está dentro de los límites de la matriz
        if 0 <= nueva_fila < len(matriz) and 0 <= nueva_columna < len(matriz[0]):
            posicion_actual = (nueva_fila, nueva_columna)
        else:
            print("Las coordenadas están fuera de los límites de la matriz.")
            break

    # Imprimir el valor de la posición final
    if 0 <= posicion_actual[0] < len(matriz) and 0 <= posicion_actual[1] < len(matriz[0]):
        print("Valor en la posición final:", matriz[posicion_actual[0]][posicion_actual[1]])
        value_boton = matriz[posicion_actual[0]][posicion_actual[1]]

    # Sumar los valores de la fila donde se encuentra el valor final
        fila_valor_final = matriz[posicion_actual[0]]
        suma_fila = sum(int(x) for x in fila_valor_final)
        print("Suma de los valores en la fila:", suma_fila)
    else:
        print("Las coordenadas resultan en una posición fuera de los límites de la matriz.")

    # Encontrar el botón con el valor resultante 
    button = driver.find_element(By.XPATH, f"//button[@value='{value_boton}']")
    # Haz clic en el botón utilizando JavaScript
    driver.execute_script("arguments[0].click();", button)


    time.sleep(3)

    # Encontrar el campo de entrada por su clase dentro del div


    # Agregar texto al campo de entrada
    input_element = driver.find_element(By.XPATH, "//html/body/div[4]/div/div/div[2]/div/div/form/input").send_keys(suma_fila)

    # Encontrar el botón por su XPath
    boton = driver.find_element(By.XPATH, "//html/body/div[4]/div/div/div[2]/div/div/form/button").click()

    num1 += 1

    wait = WebDriverWait(driver, 10)

    try:
        element = wait.until(EC.presence_of_element_located((By.XPATH, "//html/body/div[2]/div[1]/p[2]")))
        print("La página se ha cargado completamente.")
    except TimeoutException:
        print("La página no se cargó completamente en el tiempo especificado.")








