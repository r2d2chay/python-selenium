from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import requests
import re
from datetime import datetime, timedelta, time
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException



# URL del sitio web
url = "https://tasks.evalartapp.com/automatization/"

# Datos de inicio de sesión
username = "824033"
password = "10df2f32286b7120Mi00LTMzMDQyOA==30e0c83e6c29f1c3"

# Configuración del navegador
options = webdriver.ChromeOptions()
options.add_argument("--start-maximized")  # Abre el navegador maximizado
driver = webdriver.Chrome(options=options)

# Abrir la página de inicio de sesión
driver.get(url)

# Localizar el campo de usuario e ingresar el nombre de usuario
username_field = driver.find_element(By.NAME, "username")
username_field.send_keys(username)

# Localizar el campo de contraseña e ingresar la contraseña
password_field = driver.find_element(By.NAME, "password")
password_field.send_keys(password)

# Enviar el formulario de inicio de sesión
password_field.send_keys(Keys.ENTER)

# Esperar a que la página se cargue después de iniciar sesión (puedes cambiar el tiempo de espera según sea necesario)
WebDriverWait(driver, 10).until(EC.url_to_be("https://tasks.evalartapp.com/automatization/forms/test"))


num1=1
while num1 <= 10:

    # Rescatar el contenido HTML de la página después de iniciar sesión
    html_content = driver.page_source
    # Crear un objeto BeautifulSoup para analizar el contenido HTML de la página
    soup = BeautifulSoup(html_content, "html.parser")

    imagenes= soup.find_all('p', {'class': 'text-center text-xl'})

    p_tags_array = []

    for p_tag in imagenes:
        p_tags_array.append(p_tag)


    #Caso 1##################################################################

    texto = str(p_tags_array[0])
    numero_dias_match = re.search(r'(\d+)\s*d[ií]as', texto)

    # Verificar si se encontró el número de días
    if numero_dias_match:
        # Obtener el número de días encontrado
        numero_dias = int(numero_dias_match.group(1))

        # Obtener la fecha del texto
        fecha_texto = re.search(r'\d{2}/\d{2}/\d{4}', texto).group()
        fecha = datetime.strptime(fecha_texto, '%d/%m/%Y')

        # Sumar el número de días a la fecha
        fecha_resultante = fecha + timedelta(days=numero_dias)

        # Imprimir la fecha resultante
        print("Fecha resultante:", fecha_resultante.strftime('%d/%m/%Y'))
    else:
        print("No se encontró el número de días en el texto.")

    # Ahora p_tags_array contiene todos los elementos encontrados

    input_element = driver.find_element(By.NAME, "date")
    # Rellenar el valor del campo de entrada de fecha con la fecha resultante
    #input_element.clear()
    input_element.send_keys(fecha_resultante.strftime('%d-%m-%Y'))


    #caso 2#####################################################################

    texto2 = str(p_tags_array[1])
    # Buscar el número que sigue a la frase "multiplos de" utilizando expresiones regulares y convertirlo en integer
    numero = int(re.search(r'multiplos de (\d+)', texto2).group(1))

    div = soup.find('div', class_='bg-white rounded-md shadow-md p-5 flex flex-col items-center my-5 w-2/5')
    # Encontrar todas las etiquetas <label> dentro del div específico
    labels = div.find_all('label')

    # Extraer los números de las etiquetas <label> y almacenarlos en una lista
    numeros = [label.get_text().strip() for label in labels]

    multiplos = [int(num) for num in numeros if int(num) % numero == 0]

    checkboxes = driver.find_elements(By.XPATH,'//div[@class="grid grid-cols-4 gap-4 mt-4"]//input[@type="checkbox"]')

    # Itera sobre los checkboxes y marca los que tengan el valor en la lista de múltiplos
    for checkbox in checkboxes:
    
        valor_checkbox = checkbox.get_attribute("value")
        entero = int(valor_checkbox)
        if entero in multiplos:
            print('encontrados')
            checkbox = driver.find_element(By.XPATH, f'//input[@type="checkbox" and @value="{valor_checkbox}"]')
            checkbox.click()


    #caso 3#####################################################################

    dibujos = str(p_tags_array[3])
    dibujo  = str(p_tags_array[2])
    lista_dibujos = list(dibujos)
    patron = r'cuantos (\S) hay'

    # Buscar el patrón en el texto
    resultado = re.search(patron, dibujo)

    # Si se encuentra el patrón, recuperar el carácter
    if resultado:
        caracter = resultado.group(1)
        print("Carácter después de 'cuantos' y antes de 'hay':", caracter)
    else:
        print("No se encontró el patrón en el texto.")

    contador = lista_dibujos.count(caracter)

    # Obtener el elemento <input> por su nombre
    input_numero = driver.find_element(By.NAME, "number")
    # Obtener el atributo "value" del elemento <input>
    valor_actual = input_numero.get_attribute("value")
    # Actualizar el valor del atributo "value" con el valor de contador_caracter
    input_numero.send_keys(contador)


    # Caso 4 ###############################################################

    ima= soup.find_all('p', {'class': 'text-center text-xl font-bold'})

    p_tags_array1 = []

    for p_tag in ima:
        p_tags_array1.append(p_tag)

    print(p_tags_array1[0])
    operacion = str(p_tags_array1[0])

    # Expresión regular para encontrar el texto entre '>' y '='
    patron = r'>([^=]+)'

    # Buscar el texto que cumple con el patrón en el string
    resultado = re.search(patron, operacion)

    # Obtener el texto encontrado
    if resultado:
        texto_encontrado = resultado.group(1)
        print(texto_encontrado)
    else:
        print("No se encontró ningún texto que coincida con el patrón.")

    # Separar los números y los operadores matemáticos
    numeros_operadores = re.findall(r'\d+|[-+*/]', texto_encontrado)

    # Resolver la expresión matemática
    resultado = eval(''.join(numeros_operadores))

    # Imprimir el resultado
    print("El resultado de la expresión es:", resultado)

    # Encontrar el checkbox por su valor
    checkbox = driver.find_element(By.XPATH, f'//input[@type="radio" and @value="{resultado}"]')

    # Hacer clic en el checkbox para marcarlo
    checkbox.click()


    #caso 5 dar clic a enviar ########################################
    #if num1 != 3 :
    driver.find_element(By.XPATH, "//html/body/div[2]/form/div[2]/button").click()
    #elif num1 == 3 :
    #time.sleep(5)
    #    break
        

    # Espera a que aparezca un botón de "Enviar"
    wait = WebDriverWait(driver, 10)

    try:
        element = wait.until(EC.presence_of_element_located((By.XPATH, "//html/body/div[2]/form/div[2]/button")))
        print("La página se ha cargado completamente.")
    except TimeoutException:
        print("La página no se cargó completamente en el tiempo especificado.")

    num1 += 1
    # Cerrar el navegador

#driver.quit()




